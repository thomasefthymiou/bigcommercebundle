<?php

namespace Thomasefthymiou\Bundle\BigcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ThomasefthymiouBigcommerceBundle:Default:index.html.twig');
    }
}
